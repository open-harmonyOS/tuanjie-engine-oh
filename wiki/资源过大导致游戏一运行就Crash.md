# 资源过大导致游戏一运行就Crash<a name="ZH-CN_TOPIC_0000001696526918"></a>

-   [问题描述](#section8787125110457)
-   [解决方案](#section2655756134516)

## 问题描述<a name="section8787125110457"></a>

若游戏运行时出现如下错误信息：

```
10-31 14:03:35.734 18851-18871 C01302/AbilityBase com.yingxiong.harmony_dfzj.huawei W [extractor.cpp(GetFileList:273)]empty dir: resources/rawfile/Data/ScriptingAssemblies.json
10-31 14:03:37.628 18851-18871 C01302/AbilityBase com.yingxiong.harmony_dfzj.huawei E [zip_file_reader_io.cpp(ReadBuffer:51)]readfile error: /data/storage/el1/bundle/entry.hap-2
10-31 14:03:37.628 18851-18871 C01302/AbilityBase com.yingxiong.harmony_dfzj.huawei E [zip_file.cpp(ExtractToBufByName:850)]read file failed, len[0]. fileName: resources/rawfile/Data/data.tj3d, offset: 22820544
```

## 解决方案<a name="section2655756134516"></a>

错误信息说明因data.UXX3d资源文件过大，导致游戏一运行就崩溃。建议在构建游戏项目前，将一部分的资源文件放至**StreamAssets**文件夹，例如视频、纹理等，尽量减少构建后的游戏程序大小。

