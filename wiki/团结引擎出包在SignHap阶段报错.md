# 团结引擎出包在SignHap阶段报错<a name="ZH-CN_TOPIC_0000001782127781"></a>

-   [问题描述](#section205125361714)
-   [解决方案](#section136244019714)

## 问题描述<a name="section205125361714"></a>

团结引擎出HarmonyOS平台包时在SignHap阶段出现如下报错信息。

![](figures/f907cbb5666fe2513b6e58eade41de6e_1200x380.png)

## 解决方案<a name="section136244019714"></a>

1.  下载并安装JDK。
2.  在团结引擎菜单栏选择“Edit \> Preferences”，在弹窗的External Tools页签下配置OpenHarmony Java SDK，选择您安装的JDK安装路径。

    ![](figures/ab3b12f56c7b94613b7e690db1689190_1172x873.png)

