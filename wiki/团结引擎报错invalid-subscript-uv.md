# 团结引擎报错invalid subscript 'uv'<a name="ZH-CN_TOPIC_0000001693955666"></a>

-   [问题描述](#section176015395520)
-   [解决方案](#section466261017555)

## 问题描述<a name="section176015395520"></a>

使用团结引擎打开游戏工程后，**控制台**和**Shader**均出现如下报错信息：

```
invalid subscript 'uv'
```

![](figures/image-(2)-7.png)

![](figures/image-(3)-8.png)

## 解决方案<a name="section466261017555"></a>

先将varying的**uv**属性修改为**textcoord**，再在代码中将input.uv替换为input.textcoord。

![](figures/image-9.png)

