# 团结引擎构建的Hap包和APK包大小差异明显<a name="ZH-CN_TOPIC_0000001703586248"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

同一个游戏工程，团结引擎构建的Hap包大小明显大于APK包大小。

## 解决方案<a name="section172643141482"></a>

目前影响包体大小的原因主要是如下两个，请逐一排查：

-   在Build Settings弹窗检查是否选择压缩算法。

    ![](figures/07d0b8ffe14a38e694c55c413648893e_801x755.png)

-   在Inspector弹窗检查是否选择纹理压缩配置项。

    ![](figures/zh-cn_image_0000001751820105.png)

