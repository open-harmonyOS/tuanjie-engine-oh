# 集成三方SDK<a name="ZH-CN_TOPIC_0000001714855492"></a>

-   [集成华为服务](#zh-cn_topic_0000001727276661_zh-cn_topic_0000001700282521_section1647911713343)
-   [集成其他服务](#zh-cn_topic_0000001727276661_zh-cn_topic_0000001700282521_section798462843613)

## 集成华为服务<a name="zh-cn_topic_0000001727276661_zh-cn_topic_0000001700282521_section1647911713343"></a>

您的HarmonyOS游戏在华为应用市场上架前，需要集成华为游戏服务并实现获取玩家信息、防沉迷等功能，具体请参见[游戏服务开发指南](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/gameservice-getstarted-harmonyos-0000001622056588)。如果您的游戏提供商品购买能力，还需要集成应用内支付服务，具体请参见[应用内支付开发指南](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/iap-introduction-0000001425429425)。更多华为服务，请参见[HarmonyOS开发指南](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/gameservice-getstarted-harmonyos-0000001622056588)。

![](figures/zh-cn_image_0000001727278613.png)

## 集成其他服务<a name="zh-cn_topic_0000001727276661_zh-cn_topic_0000001700282521_section798462843613"></a>

如您的游戏还集成其他服务，请替换为HarmonyOS版本。如服务仍未支持HarmonyOS，请统一联系华为技术支持推动HarmonyOS适配。

