# hdc install错误码9568322<a name="ZH-CN_TOPIC_0000001745948337"></a>

-   [问题描述](#section1354917470581)
-   [原因分析](#section18510547447)
-   [解决方案](#section1066965218581)

## 问题描述<a name="section1354917470581"></a>

执行hdc install命令后出现如下报错信息：

```
failed to install bundle. code:9568322 error: signature verification failed due to not trusted app source.
```

![](figures/image-(2)---副本.png)

## 原因分析<a name="section18510547447"></a>

上述错误信息说明app source不可信，这种情况是因为测试机的UDID信息校验失败。经查看，发现当前模块使用debug类型的签名证书，debug证书需将设备UDID填到Profile文件中，这样导致签名后的游戏仅能安装在对应UDID的测试机上，安装在其它测试机上就会失败。

## 解决方案<a name="section1066965218581"></a>

我们提供如下两种解决方案：

-   **第一种**：重新申请**release**类型的签名证书。
    1.  执行如下命令，获取当前测试机的UDID。

        ```
        hdc shell bm get -u
        ```

    2.  重新申请签名文件，并将测试机的UDID填到Profile文件中，详细操作可参见[准备签名文件](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/ide-publish-app-0000001053223745#section793484619307)。

-   **第二种**：在DevEco Studio的签名弹窗勾选“Automatically generate signature”，点击下方“Apply”为游戏重新生成自动签名信息。

    ![](figures/73e13bc43757280cd441853d38f29312_879x731-(1).png)

