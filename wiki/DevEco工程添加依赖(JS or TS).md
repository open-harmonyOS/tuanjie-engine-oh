# 前言
DevEco工程可以导入JS依赖库，如axios、crypto-js等库，在DevEco工程里面需要有一些特定的步骤才能引入并使用。

# 引入依赖
根据<a href="https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/ide-ohpm-historical-project-migration-0000001559935894" target="_blank">官方文档</a>说明：
> 在DevEco Studio 3.1 Release及更高版本上新建API 9及以上版本的工程将使用`ohpm`作为默认包管理器。

如需要添加依赖，
1. 先到<a href="https://ohpm.openharmony.cn/#/cn/home" target="_blank">OpenHarmony三方库中心仓</a>找到需要的三方库信息
2. 将其信息填入到entry目录下的`oh-package.json`文件中的`"dependencies"`数组中
3. 执行``ohpm install``命令安装依赖包，依赖包会存储在工程的`oh_modules`目录下

# 举例(crypto-js)
- 在<a href="https://ohpm.openharmony.cn/#/cn/home" target="_blank">OpenHarmony三方库中心仓</a>搜索`crypto-js`，结果如下：
  ![输入图片说明](figures/ohos_crypto-js.png)
  点进去之后查看版本，选取版本，比如说`2.0.1`版本
  ![输入图片说明](figures/ohos_crypto-js_version.png)
- 信息填入entry目录下的`oh-package.json`文件中的`"dependencies"`数组中：
  ![输入图片说明](figures/ohos_crypto-js_json.png)
- 命令行执行``ohpm install``命令安装依赖包，依赖包会存储在工程的`oh_modules`目录下：
  ![输入图片说明](figures/ohos_crypto-js_oh_modules.png)
- 代码中引入使用：
  ![输入图片说明](figures/ohos_crypto-js_import.png)

# 相关资料
- <a href="https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md" target="_blank">使用OpenHarmony HAR</a>
- <a href="https://ohpm.openharmony.cn/#/cn/home" target="_blank">OpenHarmony三方库中心仓</a>
- <a href="https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/ide-ohpm-historical-project-migration-0000001559935894" target="_blank">历史工程适配OHPM包管理</a>