# Build hap\(s\)未生成build目录<a name="ZH-CN_TOPIC_0000001735088710"></a>

-   [问题描述](#section205125361714)
-   [解决方案](#section136244019714)

## 问题描述<a name="section205125361714"></a>

在DevEco Studio的菜单栏中点击**Build Hap\(s\)**后未生成build目录，且未生成Hap包。sync以及build日志如图：

![](figures/2f156b508ef380a46926caf8f570b354_1361x567.png)

![](figures/707592498fb21b52dd2708606c02fb9b_1398x643.png)

## 解决方案<a name="section136244019714"></a>

hvigor 3.0.2版本的离线插件目录中有workspace，可能出现解析异常的问题，您需升级**hvigor**版本，操作如下：

1.  请将项目工程dependencies目录下的文件用DecEco Studio开发套件内的配套的hvigor文件进行替换，即可将**hvigor**升级至3.0.9版本，操作指南请参考[配置SDK出现格式错误](配置SDK出现格式错误.md)。

    ![](figures/ce58068eac1e14c2446e9baae609ccb1_981x317.png)

2.  更新项目hvigor/hvigor-config.json5文件中的版本号。

    ![](figures/683f4a74ece5961d221982f26e67c066_1120x525.png)

