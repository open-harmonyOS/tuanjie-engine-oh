# AVPlayer属性获取失败<a name="ZH-CN_TOPIC_0000001751425973"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

游戏工程运行过程中出现如下报错信息：

![](figures/437d94a11490ae2ab0a11f092fd06ff4_950x144.png)

## 解决方案<a name="section172643141482"></a>

报错信息说明游戏工程无法获取到AVPlayer的属性duration。AVPlayer的duration属性限制只可在进入prepared之后才可获取duration，当在未进入prepared状态是获取duration会发生上述报错，在获取duration时预先进行avplayer实例的状态判断即可解决此问题。

