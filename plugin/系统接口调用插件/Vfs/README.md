# 文件系统空间统计工具(含demo)
将文件夹下的`Vfs_Plugin.package`导入到项目中，可以看到有如下几个文件：
![输入图片说明](../../../wiki/public_sys-resources/vfs_sample.png)

点击**import**导入即可
# 说明
Plugin里面含有一个demo场景`VfsDemo`，MainCamera下绑定一个Demo脚本`OpenHarmonyVfsSample.cs`，其调用Vfs工具类`OpenHarmonyStatvfsHelper.cs`，最终调用到`Plugins/OpenHarmony/StatvfsManager.tslib`，在这个文件中会调用到ArkTs接口，
参考文档为: <a href="https://developer.huawei.com/consumer/cn/doc/harmonyos-references/js-apis-file-statvfs-0000001580026202" target="_blank">@ohos.file.statvfs (文件系统空间统计)
<a/>