# 进入Fastbots模式报错FAILED Write device failed Too many links<a name="ZH-CN_TOPIC_0000001751425981"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

Mate 60型号的测试机进入Fastbots模式出现如下报错信息：

![](figures/image-(1)-(2).png)

## 解决方案<a name="section172643141482"></a>

执行一次update\_allpkg.bat，若还是出现上述报错信息，请再执行一次update\_allpkg.bat。

![](figures/image-(2)-(1).png)

