# XR组件报错<a name="ZH-CN_TOPIC_0000001676616976"></a>

-   [问题描述](#section197131595246)
-   [解决方案](#section4274947132518)

## 问题描述<a name="section197131595246"></a>

在团结引擎中，将游戏切换HarmonyOS平台时，XR组件出现如下报错：

![](figures/图层-2.png)

## 解决方案<a name="section4274947132518"></a>

打开Window-Package Manager，找到**XR Legacy Input Helpers**组件，点击**Remove**即可。

![](figures/图层-3.png)

