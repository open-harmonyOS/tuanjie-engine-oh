# Tips&操作指导<a name="ZH-CN_TOPIC_0000001698187906"></a>

-   **[游戏侧滑(或按返回键)二次确认.md](游戏侧滑(或按返回键)二次确认.md)**
  
-   **[DevEco工程添加依赖(JS or TS)](DevEco工程添加依赖(JS%20or%20TS).md)**

-   **[游戏中如何保持屏幕常亮](游戏中如何保持屏幕常亮.md)**  

-   **[游戏脚本移动平台分支HarmonyOS化处理](游戏脚本移动平台分支HarmonyOS化处理.md)**  

-   **[如何批量修改HarmonyOS平台Texture的纹理设置](如何批量修改HarmonyOS平台Texture的纹理设置.md)**  

-   **[Application路径HarmonyOS平台映射关系](Application路径HarmonyOS平台映射关系.md)**  

-   **[如何连接Profiler并查看数据](如何连接Profiler并查看数据.md)**  

-   **[如何获取包体信息](如何获取包体信息.md)**  

-   **[传文件报错read-only file system](传文件报错read-only-file-system.md)**  

-   **[使用hdc命令的场景](使用hdc命令的场景.md)**  

-   **[进入Fastbots模式报错FAILED Write device failed Too many links](进入Fastbots模式报错FAILED-Write-device-failed-Too-many-links.md)**  

-   **[如何传送大文件](如何传送大文件.md)**  

-   **[C\#如何分别调用静态库&动态库](C-如何分别调用静态库-动态库.md)**  

-   **[如何读取StreamingAssets文件夹内容](如何读取StreamingAssets文件夹内容.md)**  

-   **[不识别HDC设备](不识别HDC设备.md)**  

-   **[如何查询是否有通知权限](如何查询是否有通知权限.md)**  

-   **[如何查看游戏沙箱文件](如何查看游戏沙箱文件.md)**  

-   **[如何查看Hap包信息](如何查看Hap包信息.md)**  

