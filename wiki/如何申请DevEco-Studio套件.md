# 如何申请DevEco Studio套件<a name="ZH-CN_TOPIC_0000001703586244"></a>

-   [问题描述](#section776911315313)
-   [解决方案](#section1779018863118)

## 问题描述<a name="section776911315313"></a>

如何申请DevEco Studio套件？

## 解决方案<a name="section1779018863118"></a>

1.  打开并使用华为帐号登录[官网](https://developer.harmonyos.com/deveco-developer-suite/)，在弹出的窗口中签署协议。
2.  填写DevEco Studio套件的申请表单。

    ![](figures/20231031-105119(WeLinkPC)-3.png)

3.  提交申请后，请向华为工作人员提供信息截图，华为工作人员会尽快处理您的申请。

