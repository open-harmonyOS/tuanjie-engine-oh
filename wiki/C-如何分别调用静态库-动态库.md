# C\#如何分别调用静态库&动态库<a name="ZH-CN_TOPIC_0000001703586256"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

如何在HarmonyOS游戏工程中调用.a静态库文件或.so动态库文件。

## 解决方案<a name="section172643141482"></a>

调用.a静态库文件和.so动态库文件前，均需把文件放置到Asset/Plugins/OpenHarmony/arm64-v8a\(armeabi-v7a\)路径下。

-   **调用.a静态库文件**
    1.  在团结引擎的Inspector中设置如下：

        ![](figures/image-(2)-24.png)

    2.  代码中使用**\[DllImport\("\_\_Internal"\)\]**调用静态库。例如示例代码中，MyAddFunc\(\)和GetpKey\(\)为静态文件中的接口函数。

        ![](figures/image-(1)-25.png)

-   **调用.so动态库文件**
    1.  在团结引擎中Inspector设置如下：

        ![](figures/image-(2)-26.png)

    2.  代码中使用**\[DllImport\("soName"\)\]**，例如libmytest.so的soName为mytest。

        ![](figures/image-(3)-27.png)

