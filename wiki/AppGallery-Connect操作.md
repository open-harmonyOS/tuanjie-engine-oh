# AppGallery Connect操作<a name="ZH-CN_TOPIC_0000001762654889"></a>

-   [注册开发者帐号](#zh-cn_topic_0000001677010758_section45848447332)
-   [创建项目和应用](#zh-cn_topic_0000001677010758_section51515576338)

## 注册开发者帐号<a name="zh-cn_topic_0000001677010758_section45848447332"></a>

若您还没有实名认证的华为开发者帐号，请前往华为开发者联盟网站注册开发者帐号并完成实名认证，详细操作请参见[帐号注册认证](https://developer.huawei.com/consumer/cn/doc/start/registration-and-verification-0000001053628148)。

## 创建项目和应用<a name="zh-cn_topic_0000001677010758_section51515576338"></a>

若尚未在AppGallery Connect创建项目和应用，请参考[创建项目](https://developer.huawei.com/consumer/cn/doc/distribution/app/agc-help-createproject-0000001100334664)和[项目下添加应用](https://developer.huawei.com/consumer/cn/doc/distribution/app/agc-help-createapp-0000001146718717#section98313321213)。在项目下添加应用时要求：

-   “选择平台”选择“APP（HarmonyOS）”。
-   “支持设备”选择“手机”。
-   “应用分类”选择“游戏”。
-   “是否元服务”选择“否”。

