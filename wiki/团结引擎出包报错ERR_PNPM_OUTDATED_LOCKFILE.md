# 团结引擎出包报错ERR\_PNPM\_OUTDATED\_LOCKFILE<a name="ZH-CN_TOPIC_0000001782117413"></a>

-   [问题描述](#section205125361714)
-   [解决方案](#section136244019714)

## 问题描述<a name="section205125361714"></a>

团结引擎出包失败，出现如下报错信息：

```
ERR_PNPM_OUTDATED_LOCKFILE.
```

![](figures/5bfaf7b73f7bb50e35afb0befeb6bf85_1615x693.png)

## 解决方案<a name="section136244019714"></a>

若在构建包体的过程中出现上述报错信息，您可以尝试如下几种方案在**.npmrc**文件中增加配置：

```
lockfile=false
```

>![](public_sys-resources/icon-note.gif) **说明：** 
>Windows系统的.npmrc文件位置在C:\\Users\\\{UserName\}\\。

-   **方案一**：直接执行如下命令行，在.npmrc文件中增加配置。

    ```
    npm config set lockfile=false
    ```

-   **方案二**：若执行上述命令报错，建议直接在.npmrc文件中需要增加如下代码行：

    ```
    lockfile=false
    ```

-   **方案三**：若不能修改系统文件，建议尝试通过echo命令在.npmrc文件中增加配置。

    ```
    cat HOME/.npmrc∣grep′lockfile=false′∣∣echo′lockfile=false′>>{HOME}/.npmrc
    ```

