# 打包显示找不到node<a name="ZH-CN_TOPIC_0000001741675925"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

在团结引擎中打包失败，显示node\_module模块找不到。

## 解决方案<a name="section10358174483919"></a>

-   **方案一**：使用团结引擎前配置node.js环境变量。

    ![](figures/image-(10).png)

-   **方案二**：在团结引擎中选择Node.js的安装路径。

    ![](figures/image-(12).png)

