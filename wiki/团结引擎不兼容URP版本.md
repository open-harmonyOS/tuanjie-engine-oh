# 团结引擎不兼容URP版本<a name="ZH-CN_TOPIC_0000001751425969"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

在团结引擎中加载14.0.8版本URP的游戏工程时，出现如下报错信息：

![](figures/9782170ef3ee4fe7a0fbbbf1767e6f21_1417x113.png)

## 解决方案<a name="section172643141482"></a>

团结引擎与URP14.0.8版本不兼容，请将URP版本降至14.0.6版本。

