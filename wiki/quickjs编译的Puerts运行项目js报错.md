# quickjs编译的Puerts运行项目js报错<a name="ZH-CN_TOPIC_0000001681768200"></a>

-   [问题描述](#section2168104821413)
-   [解决方案](#section7961410150)

## 问题描述<a name="section2168104821413"></a>

使用quickjs编译出的Puerts，在运行时报js语法错误。

## 解决方案<a name="section7961410150"></a>

请按照对应的方法进行修改：

-   若RegExp.$1\~9 不适用，请参考[RegExp.$1\~9被弃用后替换方法](http://www.manongjc.com/detail/42-pyedxzrdxnsgebw.html)进行替换。

    ![](figures/image-(3)-14.png)

-   若静态变量需在类初始化之后才能初始化，请参考如下修改：

    ![](figures/image-(4).png)

-   若想**关键词get**能作为属性或变量，请参考如下修改：

    ![](figures/image-(5).png)

