# NGUI Next-Gen UI无法使用华为输入法<a name="ZH-CN_TOPIC_0000001741835781"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

使用NGUI Next-Gen UI v3.11.2时，无法使用华为输入法。

## 解决方案<a name="section10358174483919"></a>

NGUI Next-Gen UI v3.11.2是团结引擎的**付费插件**，暂未适配HarmonyOS系统。当前您可以自行修改规避，后续商务联系NGUI发布正式版本再替换。

1.  在团结引擎中自主设计NUGI输入框，或参考[输入框设计样式1](https://www.cnblogs.com/zyn95186/p/7307804.html)、[输入框设计样式2](https://www.cnblogs.com/May-day/p/8552474.html)。

    ![](figures/image-(7)-12.png)

2.  找到输入框对应的UIInput.cs脚本。

    ![](figures/image-(8).png)

    在代码中增加HarmonyOS平台的处理逻辑。

    ```
    // 6~8行：
    #if !UXX_EDITOR && (UXX_IPHONE || UXX_ANDROID || UXX_OPENHARMONY || UXX_WP8 || UXX_WP_8_1 || UXX_BLACKBERRY || UXX_WINRT || UXX_METRO)
    #define MOBILE
    #endif
    ...
    // 652~663行：
    RuntimePlatform pf = Application.platform;
    			if (pf == RuntimePlatform.IPhonePlayer
    				|| pf == RuntimePlatform.Android
    				|| pf == RuntimePlatform.OpenHarmony
    				|| pf == RuntimePlatform.WP8Player
     #if UXX_4_3
    				|| pf == RuntimePlatform.BB10Player
     #else
    				|| pf == RuntimePlatform.BlackBerryPlayer
    				|| pf == RuntimePlatform.MetroPlayerARM
    				|| pf == RuntimePlatform.MetroPlayerX64
    				|| pf == RuntimePlatform.MetroPlayerX86
     #endif
    			)
    ```

