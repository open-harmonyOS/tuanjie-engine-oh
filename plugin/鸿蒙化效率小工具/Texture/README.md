# 工具说明
将本目录下的[package](./TextureImporterHelper.unitypackage)下载到本地并且导入到项目中；如果项目中本身已有类继承于`AssetPostprocessor`，那么直接将核心逻辑放入`OnPreprocessTexture()`的覆盖实现中即可。
注意，代码中此块：
```c#
    // TODO: 设置配置值 -- 可以参考Android平台
    openHarmonySettings.format = TextureImporterFormat.ASTC_6x6;
    openHarmonySettings.maxTextureSize = 2048;
```
需要根据自己项目中其它移动平台的设置进行配置。

# 使用方法
代码导入并适配完成后，新的Texture导入会自动修改OpenHarmony平台的对应配置；对于已有的Texture，可以在搜索所有的Texture或者在文件夹处,右键选中直接**ReImport**即可
![](../../../wiki/figures/image-(3)-17.png)