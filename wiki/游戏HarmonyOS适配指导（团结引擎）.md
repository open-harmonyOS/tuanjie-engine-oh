# 游戏HarmonyOS适配指导（团结引擎）<a name="ZH-CN_TOPIC_0000001714855504"></a>

-   [业务简介](#section1962561314260)
-   [适配流程](#section10245622162615)
-   [适配准备](#section1950952714122)
-   [适配平台](#section11481639161214)
-   [适配三方库](#section43051142191210)
-   [替换系统能力](#section12337124501215)
-   [验证游戏基本逻辑](#section9313452141213)
-   [构建HarmonyOS工程](#section020918579127)
-   [集成三方SDK](#section163934614130)
-   [调试与上架](#section1948951011134)
-   [问题案例集](#section7977813111314)
-   [附录](#section13369153862)

>![](public_sys-resources/icon-note.gif) **说明：** 
>文档的“**UXX**”值请与华为技术支持进行确认。

## 业务简介<a name="section1962561314260"></a>

游戏适配HarmonyOS是指将游戏的应用程序适配HarmonyOS操作系统，以实现在HarmonyOS设备上的运行。HarmonyOS操作系统是华为公司自主研发的操作系统，具有分布式架构、多设备协同、安全可靠等特点，可以为游戏开发者提供更好的开发环境和用户体验。本文主要介绍基于**团结引擎**的游戏适配HarmonyOS方案。

## 适配流程<a name="section10245622162615"></a>

![](figures/未标题-1.png)

## 适配准备<a name="section1950952714122"></a>

为了后续顺利适配HarmonyOS平台，您需提前做好[适配准备](适配准备.md)。

## 适配平台<a name="section11481639161214"></a>

若游戏代码中出现Android、iOS等其它平台的判断，您需在分支处加上HarmonyOS平台宏定义的处理逻辑，详情请参见[适配平台](适配平台.md)。

## 适配三方库<a name="section43051142191210"></a>

不同操作系统的本质不同，库的二进制是互不兼容，因此需针对不同操作系统进行编译三方库，详情请参见[适配三方库](适配三方库.md)。

## 替换系统能力<a name="section12337124501215"></a>

游戏原有的系统方法在HarmonyOS平台可能不支持，您需调用HarmonyOS系统接口替换适配。调用HarmonyOS系统接口的原理及样例请参见[替换系统能力](替换系统能力.md)。

## 验证游戏基本逻辑<a name="section9313452141213"></a>

建议您从团结引擎中构建Hap包，在测试机上测试游戏的基本逻辑无问题后，再在引擎中导出HarmonyOS工程，前往DevEco Studio中打开，操作详情请参见[验证游戏基本逻辑](验证游戏基本逻辑.md)。

## 构建HarmonyOS工程<a name="section020918579127"></a>

在团结引擎中导出HarmonyOS工程后，再在DevEco Studio中打开，操作详情请参见[构建HarmonyOS工程](构建HarmonyOS工程.md)。

## 集成三方SDK<a name="section163934614130"></a>

HarmonyOS游戏在华为应用市场上架前，需集成华为游戏服务并实现获取玩家信息、防沉迷等功能，详情请参见[集成三方SDK](集成三方SDK.md)。
## 签名
针对应用/服务的签名，DevEco Studio为开发者提供了自动签名方案，帮助开发者高效进行调试。也可选择手动方式对应用/服务进行签名。[DevEco签名](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V2/ide-signing-0000001587684945-V2) （需要申请权限）
## 调试与上架<a name="section1948951011134"></a>

运行并调试HarmonyOS游戏的功能和性能，并前往AppGallery Connect提交上架申请，具体操作请参见[调试与上架](调试与上架.md)。

## 问题案例集<a name="section7977813111314"></a>

若遇到问题，请先仔细阅读文档，若仍未解决您的问题，请前往[问题案例集](问题案例集.md)进行查看。

## 附录<a name="section13369153862"></a>

前往[附录](附录.md)，了解更多信息。

