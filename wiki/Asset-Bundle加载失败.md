# Asset Bundle加载失败<a name="ZH-CN_TOPIC_0000001703622630"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

游戏工程加载Asset Bundle时出现如下报错信息：

```
failed to load assetbundle the assetbundle cant be loaded because it was not built with the right version or build target.
```

## 解决方案<a name="section172643141482"></a>

出现上述问题是因为加载的Asset Bundle为非HarmonyOS系统的打包工具，您需重新将HarmonyOS系统平台的Asset Bundle工具加入到游戏工程中。

