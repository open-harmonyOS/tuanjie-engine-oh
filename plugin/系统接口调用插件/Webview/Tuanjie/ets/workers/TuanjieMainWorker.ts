import worker from '@ohos.worker';
import sensor from '@ohos.sensor'
import { Constants } from '../common/Constants';
import { TuanjieLog } from '../common/TuanjieLog';
import { TuanjiePermissions } from '../utils/TuanjiePermissions'
import { TuanjieLocation } from '../utils/TuanjieLocation'
import { WindowUtils } from '../utils/WindowUtils'
import tuanjie from 'libtuanjie.so'
import vibrator from '@ohos.vibrator';
import display from '@ohos.display';
import business_error from '@ohos.base';


export class TuanjieMainWorker {
  public threadWorker: worker.ThreadWorker;

  private constructor() {
    TuanjieLog.debug('%{public}s', 'TuanjieMainWorker.constructor');
    this.threadWorker = new worker.ThreadWorker("entry/ets/workers/TuanjieMainWorkerHandler.ts");
    this.threadWorker.onerror = function (e) {
      var msg = e.message;
      var filename = e.filename;
      var lineno = e.lineno;
      var colno = e.colno;
      TuanjieLog.error(`TuanjieMainWorker Error ${msg} ${filename} ${lineno} ${colno}`);
    }

    this.threadWorker.onmessage = async function (msg) {
      if (msg.data.type == "RUN_ON_UI_THREAD") {
        tuanjie.processUIThreadMessage();
      }
      if (msg.data.type == "RUN_ON_UI_THREAD_JS") {
        TuanjieLog.info('%{public}s', 'RUN_ON_UI_THREAD_JS start！');

        var funcName = msg.data.funcName;

        if (funcName == "ShowSoftInput") {
          var initialText = msg.data.initialText;

          globalThis.inputInitialText = initialText;
          //TuanjieLog.info('%{public}s', 'dialogController open start');
          globalThis.dialogController.open();
          //TuanjieLog.info('%{public}s', 'dialogController open finish');

          TuanjieLog.info(msg.data.funcName);
        }

        if (funcName == "RequestUserPermissions") {
          TuanjiePermissions.requestUserPermissions(msg.data.permissions, msg.data.onGranted, msg.data.onDenied);
        }

        if (funcName == "CheckPermission") {
          TuanjiePermissions.checkPermission(msg.data.permission, msg.data.onAuthorized, msg.data.onUnauthorized);
        }

        if (funcName == "HasPermission") {
          TuanjiePermissions.hasPermission(msg.data.permission, msg.data.userData, msg.data.timeoutMs);
        }

        if (funcName == "LocationUpdates") {
          TuanjieLocation.requestLocationUpdates(msg.data.timeInterval, msg.data.distanceInterval, msg.data.accuracy);
        }

        if (funcName == "RemoveUpdates") {
          TuanjieLocation.removeUpdates();
        }

        if (funcName == "GetLastKnownLocation") {
          TuanjieLocation.getLastKnownLocation(msg.data.userData);
        }

        if (funcName == "GetDeclination") {
          TuanjieLocation.getDeclination(msg.data.userData, msg.data.timeoutMs, msg.data.latitude, msg.data.longitude, msg.data.altitude, msg.data.timestamp);
        }

        if (funcName == "SetSystemBarState") {
          WindowUtils.setSystemBarState(msg.data.systemBars);
        }

        if (funcName == "SetOrientation") {
          WindowUtils.setOrientation(msg.data.orientation);
        }

        if (funcName == "EnableSensor") {
          var sensorType = msg.data.sensorType;
          var sensorRate = msg.data.sensorRate;
          try {
            sensor.on(sensorType, function (data) {
              //TuanjieLog.info('------sensor sensorType: ' + sensorType);
              //TuanjieLog.info('------sensor X-coordinate component: ' + data.x);
              //TuanjieLog.info('------sensor Y-coordinate component: ' + data.y);
              //TuanjieLog.info('------sensor Z-coordinate component: ' + data.z);

              const sensorData = new Map();
              sensorData.set("sensorType", sensorType);
              sensorData.set("x", data.x);
              sensorData.set("y", data.y);
              sensorData.set("z", data.z);
              TuanjieMainWorker.getInstance().postMessage({ type: 'OnSensor', data: sensorData });
            }, { interval: sensorRate });
          } catch (err) {
            console.error('------sensor On fail, errCode: ' + err.code + ' ,msg: ' + err.message);
          }
        }

        if (funcName == "Vibrate") {
          var vibrateMs = msg.data.time;
          try {
            vibrator.startVibration({
              type: 'time',
              duration: vibrateMs,
            }, {
              id: 0,
              usage: 'alarm'
            }, (error) => {
              if (error) {
                console.error('Vibrate fail, error.code: ' + error.code + 'error.message: ', +error.message);
                return;
              }
              console.log('Callback returned to indicate a successful vibration.');
            });
          } catch (err) {
            console.error('errCode: ' + err.code + ' ,msg: ' + err.message);
          }

          TuanjieLog.info(msg.data.funcName);
        }

        // webview
        if (funcName == "CreateWebView") {
          globalThis.webviewInfo.occupyble = true;
        }

        if (funcName == "RemoveWebview") {
          globalThis.webviewInfo.reset();
        }

        if (funcName == "LoadURL") {
          globalThis.webviewInfo.url = msg.data.url;
          globalThis.webviewInfo.controller.loadUrl(msg.data.url);
        }

        if (funcName == "LoadHTMLString") {
          globalThis.webviewInfo.controller.loadData(msg.data.contents, "text/html", "UTF-8", msg.data.baseUrl);
        }

        if (funcName == "LoadData") {
          globalThis.webviewInfo.controller.loadData(msg.data.contents, "text/html", "UTF-8", msg.data.baseUrl);
        }

        if (funcName == "EvaluateJS") {
          try {
            globalThis.webviewInfo.controller.runJavaScript(msg.data.jsContents).then((result) => {
              if (result) {
                this.webResult = result
                console.info(`The return value is: ${result}`)
              }
            })
          } catch (error) {
            let e: business_error.BusinessError = error as business_error.BusinessError;
            console.error(`ErrorCode:${e.code}, Message:${e.message}`);
          }
        }

        if (funcName == "SetVisibility") {
          globalThis.webviewInfo.visible = msg.data.visible;
        }

        if (funcName == "SetMargins") {
          let viewInfo = globalThis.webviewInfo;
          viewInfo.x = msg.data.ml;
          viewInfo.y = msg.data.mt;
          let w = display.getDefaultDisplaySync().width - msg.data.ml - msg.data.mr;
          let h = display.getDefaultDisplaySync().height - msg.data.mt - msg.data.mb;
          viewInfo.w = w;
          viewInfo.h = h;
        }

        if (funcName == "Reload") {
          globalThis.webviewInfo.controller.refresh();
        }

        if (funcName == "StopLoading") {
          globalThis.webviewInfo.controller.stop();
        }

        if (funcName == "GoForward") {
          if (globalThis.webviewInfo.controller.accessForward()) {
            globalThis.webviewInfo.controller.forward()
          }
        }

        if (funcName == "GoBack") {
          if (globalThis.webviewInfo.controller.accessBackward()) {
            globalThis.webviewInfo.controller.backward()
          }
        }
      }
    }
  }

  public static getInstance(): worker.ThreadWorker {
    if (AppStorage.Get(Constants.APP_KEY_TUANJIE_MAIN_WORKER) == null) {
      AppStorage.SetOrCreate(Constants.APP_KEY_TUANJIE_MAIN_WORKER, new TuanjieMainWorker);
    }
    var tuanjieMainWorker = AppStorage.Get(Constants.APP_KEY_TUANJIE_MAIN_WORKER) as TuanjieMainWorker;
    return tuanjieMainWorker.threadWorker;
  }
}

