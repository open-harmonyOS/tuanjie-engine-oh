# 背景
团结引擎鸿蒙化第一课，请开发者 **一定要认真阅读** ；

主要包括以下工作：
- 团结引擎环境配置，以及DevEco的下载、环境配置
- 团结引擎工程配置、直接出包流程
- 团结引擎导出DevEco工程，以及出包流程
***
# 0. 前置准备
- [下载团结引擎](https://unity.cn/tuanjie/releases)，先下载TuanjieHub，安装Editor和OpenHarmony平台支持即可。
- [团结引擎OpenHarmony文档参考](https://docs.unity.cn/cn/tuanjiemanual/Manual/openharmony.html)
- 申请开发者套件
    1.  打开并使用华为帐号登录[官网](https://developer.harmonyos.com/deveco-developer-suite/)，在弹出的窗口中签署协议。
    2.  请认真填写DevEco Studio开发套件的申请表单。
     ![](../wiki/figures/20231031-105119(WeLinkPC).png)
    3.  提交申请后，请向华为工作人员提供信息截图，华为工作人员会尽快处理您的申请。
- 下载开发者套件中的4.0.3.700版本
![输入图片说明](images/Deveco_4.0.3.700.png)
- [配置系统变量](https://gitee.com/swallowguo/tuanjie-engine-oh/blob/master/wiki/%E9%85%8D%E7%BD%AE%E7%B3%BB%E7%BB%9F%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F.md)
- 下载本目录下的 **HeadFirstPackage.zip** ,并且解压，里面的.package文件或者.unitypackage文件均可导入团结引擎中
![输入图片说明](images/image.png)
- 使用团结引擎新建一个空项目，并且将上一步下载的插件导入进去。
![输入图片说明](images/importPacakge.png)
# 1. 环境配置
## 1.1 DevEco环境配置
准备工作中下载的4.0.3.700版本的DevEco开发套件是一个zip文件，解压之后可以看到如下文件：![输入图片说明](images/deveco_4.0.3.700_unzip.png)
> 请一定要 **认真阅读** 《DevEco Studio环境配置指导》并且按照文中指导进行 **开发环境流程搭建和配置** 。
- OpenHarmony SDK 配置
按照《DevEco Studio环境配置指导》操作之后，另外也需要配置一下 OpenHarmony SDK；
    1. 新建OpenHarmony SDK 的目录，如(D:\software\OpenHamony_SDK)
    2. Tools-SDK Manager 打开 SDK Manager工具

        ![输入图片说明](images/Tools_sdkManagerimage.png)
    3. 打开HarmonyOS_SDK配置的目录，将如图红圈部分的子文件都拷贝到前面新建的OpenHarmony SDK 的目录下
        ![输入图片说明](images/HarmonyOS_SDK_detail.png)
        ![输入图片说明](images/sdk_detail.png)

        拷贝后：
        ![输入图片说明](images/01_OpenHarmony_sdk_detailimage.png)
    4. 将其配置到SDK Manager中
        ![输入图片说明](images/02_sdkManager_openHarmonyimage.png)
## 1.2 团结引擎环境配置
打开"Edit-Preferences-External Tools"，进行环境配置

![输入图片说明](images/Preferencesimage.png),
![输入图片说明](images/03_ExternalTools.png)

- OpenHarmony SDK 配置的就是上一步中的 OpenHarmony SDK 的目录，注意，需要精确到具体版本号，如上图
- Node.js的安装以及JDK的安装，在前期准备中都有提到，配置到具体地址即可。

# 2. 切换平台
打开"File-Build Settings", 点击OpenHarmony，然后点击**Switch Platform**:
![输入图片说明](images/04_switchPlatform.png) 
如果工程较大，这一步会消耗较多时间，请耐心等待。

## 2.1 PlayerSettings配置修改
打开[Player Settings] - [Other Settings] - [Target Architecture]，仅勾选  **ARM64** , 配置如下：
![输入图片说明](images/18_arch.png)

# 3. 构建Hap包
## 3.1 直接出包
![输入图片说明](images/105_BuildHap.png)

弹出窗口，选择出包放置的目录并填写Hap包名称，之后等待出包即可

## 3.2 导出工程并使用DevEco出包
### 3.2.1 导出工程
如图所示，勾选"Export Project", 之后选择目录，即可导出团结引擎过程对应的DevEco工程：
![输入图片说明](images/06_ExportProject.png)
### 3.2.2 DevEco Studio 打开工程，解决项目配置问题
- 修改项目级build-profile.json5文件

    修改前：![输入图片说明](images/07_buildJson5Beforeimage.png)
    修改后：![输入图片说明](images/08_buildJson5After.png)
文本：
    ```json  
     "products": [
      {
        "name": "default",
        "signingConfig": "default",
        "compileSdkVersion": "4.0.0(10)",
        "compatibleSdkVersion": "4.0.0(10)",
        "targetSdkVersion": "4.0.0(10)",
        "runtimeOS": "HarmonyOS",
      },
    ]
    ```
- 配置插件
    1. 将开发者套件4.0.3.700解压后的 **dependencies**文件夹放到工程目录中。
    ![输入图片说明](images/00_devecoFiles.png)
    ![输入图片说明](images/09_dependencies.png)
    2. 打开hvigor目录下的 **hvigor-config.json5** 文件，修改 **hvigorVersion** 和 **dependencies** 字段内的信息。
    ![输入图片说明](images/10_hvigiorConfig.png)
    ```json
    {
      "hvigorVersion": "file:D:\\software\\deveco4\\hvigor-3.0.10-s.tgz",
      "dependencies": {
        "@ohos/hvigor-ohos-plugin": "file:D:\\software\\deveco4\\hvigor-ohos-plugin-3.0.10-s.tgz",
        "rollup": "file:D:\\software\\deveco4\\rollup.tgz",
      }
    }
    ```
    修改完之后，点击右上角 Try Again，
    ![输入图片说明](images/11_hvigorTryAgainimage.png)
    安装上述配置的插件，等待一段时间后，插件配置完成。
    
### 3.2.3 签名
点击 **File > Project Structure... > Project > SigningConfigs** 界面勾选“ **Automatically generate signature** ”，等待自动签名完成后，点击“OK”即可。如下图所示:
![输入图片说明](images/12_signHap.png)
### 3.2.4 出包
在菜单栏点击 **Build > Build Hap(s)/APP(s) > Build Hap(s)** , 如下图所示：
![输入图片说明](images/13_BuildHap.png)
提示“BUILD SUCCESSFUL”，表示构建成功。
![输入图片说明](images/14_BuildHapsSuc.png)
构建后的Hap包路径在`entry/build/default/outputs/default`目录下，安装到手机上的是`entry-default-signed.hap`
![输入图片说明](images/15_HapsPath.png)

# 4. 安装Hap包到手机
将搭载单框架鸿蒙系统的真机与电脑连接，启动cmd, 在cmd中输入
```shell
hdc install **.hap
```
等待安装完成即可
![输入图片说明](images/16_installHap.png)

# 5. 打开应用
在手机上点开应用，屏幕中间显示"Hello OpenHarmony",即代表demo成功了。
![输入图片说明](images/17_demo.png)