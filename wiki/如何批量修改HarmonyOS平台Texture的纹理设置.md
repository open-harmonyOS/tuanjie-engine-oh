# 如何批量修改HarmonyOS平台Texture的纹理设置<a name="ZH-CN_TOPIC_0000001733796242"></a>

-   [问题描述](#section06061365373)
-   [解决方案](#section873759173710)

## 问题描述<a name="section06061365373"></a>

如何批量修改HarmonyOS平台的纹理设置，可以减少安装包的大小，针对平台做特殊处理等。

## 解决方案<a name="section873759173710"></a>

纹理Texture的设置关乎平台的特性设置，设置详情请参见[Texture Import Settings官方文档](https://docs.unity3d.com/cn/2022.2/Manual/class-TextureImporter.html)。设置纹理Texture会影响到最终构建的包体大小，若不对HarmonyOS平台特定设置，会发现Hap包相对于APK包的体积大很多。

![](figures/PlatformSpecificOverrides.png)

因此需参考其他移动平台对HarmonyOS平台的纹理设置做统一处理。在修改纹理设置时，对于大量的纹理，相对于手动修改，建议使用脚本进行统一修改。实现逻辑是继承**AssetPostprocessor**，重写OnPreprocessTexture方法，并添加HarmonyOS平台分支的处理，示例代码如下：

```
using UXXEditor;
public class TextureImporter : AssetPostprocessor
{
    private void OnPreprocessTexture()
    {
        UXXEditor.TextureImporter textureImporter = (UXXEditor.TextureImporter)assetImporter;
#if UXX_OPENHARMONY
        TextureImporterPlatformSettings openHarmonySettings =
            textureImporter.GetPlatformTextureSettings("OpenHarmony");
        openHarmonySettings.overridden = true;

        // 设置配置值
        openHarmonySettings.format = TextureImporterFormat.ASTC_6x6;
        openHarmonySettings.maxTextureSize = 2048;

        textureImporter.SetPlatformTextureSettings(openHarmonySettings);
#endif
    }
}
```

脚本写好后，HarmonyOS平台会自动处理新导入的Texture。您也可以右键**ReImport**，批量选择已有的Texture。

![](figures/image-(3)-17.png)

