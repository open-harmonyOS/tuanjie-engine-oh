import worker from '@ohos.worker';

const workerPort = worker.workerPort;

export class WebviewControl {
  private static KWebTag: number = 0;
  private webTag: number = -1;

  public constructor() {
    this.webTag = WebviewControl.KWebTag++;
  }

  public CreateWebview() {
    console.info('>>>> constructor method enter');
    workerPort.postMessage({ type: "RUN_ON_UI_THREAD_JS", funcName: "CreateWebView", webTag: this.webTag });
  }

  public RemoveWebview(): void {
    workerPort.postMessage({ type: "RUN_ON_UI_THREAD_JS", funcName: "RemoveWebview", webTag: this.webTag });
  }

  public LoadURL(url: string): void {
    console.info('>>>> LoadURL method enter url is ' + url);
    workerPort.postMessage({ type: "RUN_ON_UI_THREAD_JS", funcName: "LoadURL", webTag: this.webTag, url: url });
  }

  public LoadHTMLString(contents: string, baseUrl: string): void {
    console.info('>>>> LoadHTMLString method enter  is ');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "LoadHTMLString",
      webTag: this.webTag,
      contents: contents,
      baseUrl: baseUrl
    });
  }

  public LoadData(contents: string, baseUrl: string): void {
    console.info('>>>> LoadData method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "LoadData",
      webTag: this.webTag,
      contents: contents,
      baseUrl: baseUrl
    });
  }

  public EvaluateJS(jsContents: string): void {
    console.info('>>>> EvaluateJS method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "EvaluateJS",
      webTag: this.webTag,
      jsContents: jsContents
    });
  }

  public Reload(): void {
    console.info('>>>> reload method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "Reload",
      webTag: this.webTag
    });
  }

  public StopLoading(): void {
    console.info('>>>> stoploading method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "StopLoading",
      webTag: this.webTag
    });
  }

  public GoForward(): void {
    console.info('>>>> goforward method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "GoForward",
      webTag: this.webTag
    });
  }

  public GoBack(): void {
    console.info('>>>> goback method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "GoBack",
      webTag: this.webTag
    });
  }

  public SetVisibility(visible: boolean): void {
    console.info('>>>> SetVisibility method enter, visible is ' + visible);
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "SetVisibility",
      webTag: this.webTag,
      visible: visible
    });
  }

  public SetMargins(ml: number, mt: number, mr: number, mb: number): void {
    console.info('>>>> SetMargins method enter');
    workerPort.postMessage({
      type: "RUN_ON_UI_THREAD_JS",
      funcName: "SetMargins",
      webTag: this.webTag,
      ml: ml,
      mt: mt,
      mr: mr,
      mb: mb
    });
  }
}

