# 如何连接Profiler并查看数据<a name="ZH-CN_TOPIC_0000001752289965"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

游戏运行在测试机上，如何连接团结引擎的Profiler工具，并查看游戏运行时的性能数据。

## 解决方案<a name="section172643141482"></a>

1.  团结引擎构建Hap调试包时，勾选**Development Build**和**Autoconnect Profiler**。

    ![](figures/image-(5)-18.png)

2.  USB数据线连接PC机和测试机。
3.  执行如下命令，打开监听端口。

    ```
    hdc fport tcp:55000 tcp:55000
    ```

    ![](figures/image-(6)-19.png)

4.  在团结引擎主界面按组合键“ctrl+7”打开profile窗口，输入**127.0.0.1**后点击“Connect”，等待端口的连接。

    ![](figures/image-(7)-20.png)

5.  在测试机上打开并运行游戏。
6.  继续打开Profile窗口，并在窗口中输入127.0.0.1，界面有变化代表成功采集到游戏的性能数据。

    ![](figures/image-(8)-21.png)

