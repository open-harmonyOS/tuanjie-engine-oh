# hdc install错误码9568347<a name="ZH-CN_TOPIC_0000001741675913"></a>

-   [问题描述](#section36019390395)
-   [定位过程](#section1925043919411)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

执行hdc install命令后出现如下报错信息：

```
[Info]App install path:E:\projects\nguiInputDemo\test.hap, queuesize:0, msg:error: failed to install bundle. code:9568347 error: install parse native so failed.
AppMod finish.
```

## 定位过程<a name="section1925043919411"></a>

根据报错信息判断是so文件解析错误。在命令行窗口执行如下命令，若结果返回64则测试机为64位系统，否则测试机为32位系。

```
hdc shell getconf LONG\_BIT
```

64位/32位系统分别对应如下指令集：

<a name="table438221664215"></a>
<table><thead align="left"><tr id="row11433142512442"><th class="cellrowborder" valign="top" width="30%" id="mcps1.1.3.1.1"><p id="p174331725154417"><a name="p174331725154417"></a><a name="p174331725154417"></a>系统</p>
</th>
<th class="cellrowborder" valign="top" width="70%" id="mcps1.1.3.1.2"><p id="p1643316253441"><a name="p1643316253441"></a><a name="p1643316253441"></a>架构</p>
</th>
</tr>
</thead>
<tbody><tr id="row63829165422"><td class="cellrowborder" valign="top" width="30%" headers="mcps1.1.3.1.1 "><p id="p1838221624218"><a name="p1838221624218"></a><a name="p1838221624218"></a>ARM 32位（armv7）</p>
</td>
<td class="cellrowborder" valign="top" width="70%" headers="mcps1.1.3.1.2 "><a name="ul3230858114420"></a><a name="ul3230858114420"></a><ul id="ul3230858114420"><li>armv7架构</li><li>armv6架构</li><li>armv5架构</li></ul>
</td>
</tr>
<tr id="row8383616174219"><td class="cellrowborder" valign="top" width="30%" headers="mcps1.1.3.1.1 "><p id="p13832164426"><a name="p13832164426"></a><a name="p13832164426"></a>ARM 64位（arm64）</p>
</td>
<td class="cellrowborder" valign="top" width="70%" headers="mcps1.1.3.1.2 "><p id="p153831016174215"><a name="p153831016174215"></a><a name="p153831016174215"></a>armv8架构（大于v8的都是64位的）</p>
</td>
</tr>
</tbody>
</table>

因32位系统和64位系统的so文件不通用，所以安装hap包时会报so解析错误。

## 解决方案<a name="section10358174483919"></a>

当前测试机为64位，您应仅勾选ARM64。

![](figures/image-(2)-10.png)

