import worker from '@ohos.worker';
import sensor from '@ohos.sensor'
import { Constants } from '../common/Constants';
import { TuanjieLog } from '../common/TuanjieLog';
import { TuanjiePermissions } from '../utils/TuanjiePermissions'
import { TuanjieLocation } from '../utils/TuanjieLocation'
import { WindowUtils } from '../utils/WindowUtils'
import tuanjie from 'libtuanjie.so'
import vibrator from '@ohos.vibrator';
import hilog from '@ohos.hilog';
import { SDKHandler } from '@ohos/hmssdk';

export class TuanjieMainWorker {
  public threadWorker: worker.ThreadWorker;

  private constructor() {
    TuanjieLog.debug('%{public}s', 'TuanjieMainWorker.constructor');
    this.threadWorker = new worker.ThreadWorker("entry/ets/workers/TuanjieMainWorkerHandler.ts");
    this.threadWorker.onerror = function (e) {
      var msg = e.message;
      var filename = e.filename;
      var lineno = e.lineno;
      var colno = e.colno;
      TuanjieLog.error(`TuanjieMainWorker Error ${msg} ${filename} ${lineno} ${colno}`);
    }

    this.threadWorker.onmessage = async function (msg) {
      hilog.info(0x0000, "SDKManager", 'UnityMainworker : %{public}s', JSON.stringify(msg));
      if (msg.data.type == "Login_SDK") {
        let result = await SDKHandler.getInstance().handleLogin(globalThis.AbilityContext);
        hilog.info(0x0000, "LoginSDK", 'login success: %{public}s', JSON.stringify(result));
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "Login", 'data': result });
      }
      if (msg.data.type == "IAP_Init") {
        let result = await SDKHandler.getInstance().handleInitIAP(globalThis.AbilityContext);
        // 向worker线程发送消息
        hilog.info(0x0000, "SDKManager", 'HMSSDKSyncToWorker send message' + result);
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "IAPInit", 'data': result });
      }
      if (msg.data.type == "SET_PUB_KEY") {
        let param = msg.data.data;
        SDKHandler.getInstance().SetPubKey(param);
      }
      if (msg.data.type == "IAP_QueryProducts") {
        let param = JSON.parse(msg.data.data);
        let result = await SDKHandler.getInstance().handleQueryIAPList(param.productType, param.productIds, globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "QueryList", 'data': result });
      }
      if (msg.data.type == "IAP_Purchase") {
        let param = JSON.parse(msg.data.data);
        let result = await SDKHandler.getInstance().handleStartPurchase(param.productType, param.productId, globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "StartPurchase", 'data': result });
      }
      if (msg.data.type == "IAP_ConsumePurchase") {
        let parameter = JSON.parse(msg.data.data);
        let result = await SDKHandler.getInstance().handleConsumePurchase(parameter.purchaseToken, globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "ConsumePurchase", 'data': result });
      }
      if (msg.data.type == "IAP_QueryOwnedPurchases") {
        hilog.info(0x0001, "Iap Demo", 'IAP_QueryOwnedPurchases msg.data.data is: ' + msg.data.data);
        let param = JSON.parse(msg.data.data);
        let result = await SDKHandler.getInstance().handleCheckOwnedPurchases(param.productType, globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({
          'type': 'syncHMSSDKResult',
          'data_type': "CheckOwnedPurchases",
          'data': result
        });
      }
      if (msg.data.type == "GamePlayer_Init") {
        let result = await SDKHandler.getInstance().handleGamePlayerInit(globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "GamePlayerInit", 'data': result });
      }
      if (msg.data.type == "GamePlayer_GetLocalPlayer") {
        let result = await SDKHandler.getInstance().handleGamePlayerGetLocal(globalThis.AbilityContext);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "getLocalPlayer", 'data': result });
      }
      if (msg.data.type == "GamePlayer_SavePlayerRole") {
        let result = await SDKHandler.getInstance().handleGamePlayerSave(msg.data.data, globalThis.AbilityContext);
        hilog.info(0x0001, "SDKManager", 'GamePlayer_SavePlayerRole msg.data.data is: ' + result);
        // 向worker线程发送消息
        TuanjieMainWorker.getInstance().postMessage({ 'type': 'syncHMSSDKResult', 'data_type': "savePlayerRole", 'data': result });
      }
      if (msg.data.type == "RUN_ON_UI_THREAD") {
        tuanjie.processUIThreadMessage();
      }
      if (msg.data.type == "RUN_ON_UI_THREAD_JS") {
        TuanjieLog.info('%{public}s', 'RUN_ON_UI_THREAD_JS start！');

        var funcName = msg.data.funcName;

        if (funcName == "ShowSoftInput") {
          var initialText = msg.data.initialText;

          globalThis.inputInitialText = initialText;
          //TuanjieLog.info('%{public}s', 'dialogController open start');
          globalThis.dialogController.open();
          //TuanjieLog.info('%{public}s', 'dialogController open finish');

          TuanjieLog.info(msg.data.funcName);
        }

        if (funcName == "RequestUserPermissions") {
          TuanjiePermissions.requestUserPermissions(msg.data.permissions, msg.data.onGranted, msg.data.onDenied);
        }

        if (funcName == "CheckPermission") {
          TuanjiePermissions.checkPermission(msg.data.permission, msg.data.onAuthorized, msg.data.onUnauthorized);
        }

        if (funcName == "HasPermission") {
          TuanjiePermissions.hasPermission(msg.data.permission, msg.data.userData, msg.data.timeoutMs);
        }

        if (funcName == "LocationUpdates") {
          TuanjieLocation.requestLocationUpdates(msg.data.timeInterval, msg.data.distanceInterval, msg.data.accuracy);
        }

        if (funcName == "RemoveUpdates") {
          TuanjieLocation.removeUpdates();
        }

        if (funcName == "GetLastKnownLocation") {
          TuanjieLocation.getLastKnownLocation(msg.data.userData);
        }

        if (funcName == "GetDeclination") {
          TuanjieLocation.getDeclination(msg.data.userData, msg.data.timeoutMs, msg.data.latitude, msg.data.longitude, msg.data.altitude, msg.data.timestamp);
        }

        if (funcName == "SetSystemBarState") {
          WindowUtils.setSystemBarState(msg.data.systemBars);
        }

        if (funcName == "SetOrientation") {
          WindowUtils.setOrientation(msg.data.orientation);
        }

        if (funcName == "EnableSensor") {
          var sensorType = msg.data.sensorType;
          var sensorRate = msg.data.sensorRate;
          try {
            sensor.on(sensorType, function (data) {
              //TuanjieLog.info('------sensor sensorType: ' + sensorType);
              //TuanjieLog.info('------sensor X-coordinate component: ' + data.x);
              //TuanjieLog.info('------sensor Y-coordinate component: ' + data.y);
              //TuanjieLog.info('------sensor Z-coordinate component: ' + data.z);

              const sensorData = new Map();
              sensorData.set("sensorType", sensorType);
              sensorData.set("x", data.x);
              sensorData.set("y", data.y);
              sensorData.set("z", data.z);
              TuanjieMainWorker.getInstance().postMessage({ type: 'OnSensor', data: sensorData });
            }, { interval: sensorRate });
          } catch (err) {
            console.error('------sensor On fail, errCode: ' + err.code + ' ,msg: ' + err.message);
          }
        }

        if (funcName == "Vibrate") {
          var vibrateMs = msg.data.time;
          try {
            vibrator.startVibration({
              type: 'time',
              duration: vibrateMs,
            }, {
              id: 0,
              usage: 'alarm'
            }, (error) => {
              if (error) {
                console.error('Vibrate fail, error.code: ' + error.code + 'error.message: ', +error.message);
                return;
              }
              console.log('Callback returned to indicate a successful vibration.');
            });
          } catch (err) {
            console.error('errCode: ' + err.code + ' ,msg: ' + err.message);
          }

          TuanjieLog.info(msg.data.funcName);
        }
      }
    }
  }

  public static getInstance(): worker.ThreadWorker {
    if (AppStorage.Get(Constants.APP_KEY_TUANJIE_MAIN_WORKER) == null) {
      AppStorage.SetOrCreate(Constants.APP_KEY_TUANJIE_MAIN_WORKER, new TuanjieMainWorker);
    }
    var tuanjieMainWorker = AppStorage.Get(Constants.APP_KEY_TUANJIE_MAIN_WORKER) as TuanjieMainWorker;
    return tuanjieMainWorker.threadWorker;
  }
}