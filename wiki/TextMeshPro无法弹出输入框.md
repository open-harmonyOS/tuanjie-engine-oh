# TextMeshPro无法弹出输入框<a name="ZH-CN_TOPIC_0000001693796186"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

使用团结引擎的文本渲染工具TextMeshPro时，无法弹出输入框。

## 解决方案<a name="section10358174483919"></a>

TextMeshPro虽然已适配HarmonyOS系统，但未同步到packageManager上，目前可以暂时规避。

-   方案一：
    1.  找到并删除工程/Library/PackageCache路径下的**com.UXX.textmeshpro**文件。
    2.  您可以前往[Gitee网站](https://gitee.com/swallowguo/tuanjie-engine-oh/blob/master/wiki/com.unity.textmeshpro@3.0.6.rar)下载已适配HarmonyOS的textmeshpro文件，解压后放至工程/packages路径下。

-   方案二：
    1.  剪切工程/Library/PackageCache路径下的com.UXX.textmeshpro文件，拷贝至工程/packages路径下。
    2.  您可以参考如下示例代码自行修改TMP\_InputField.cs文件：

        ```
        public bool shouldHideSoftKeyboard
        {
        	get
        	{
        		switch (Application.platform)
        		{
        			case RuntimePlatform.Android:
        			case RuntimePlatform.IPhonePlayer:
        			case RuntimePlatform.tvOS:
        			case RuntimePlatform.WSAPlayerX86:
        			case RuntimePlatform.WSAPlayerX64:
        			case RuntimePlatform.WSAPlayerARM:
        			case RuntimePlatform.Stadia:
        			case RuntimePlatform.OpenHarmony:
        			#if UXX_2020_2_OR_NEWER
        			case RuntimePlatform.PS4:
        				#if !(UXX_2020_2_1 || UXX_2020_2_2)
        				case RuntimePlatform.PS5:
        				#endif
        			#endif
        			case RuntimePlatform.Switch:
        				return m_HideSoftKeyboard;
        			default:
        				return true;
        		}
        	}
        
        	set
        	{
        		switch (Application.platform)
        		{
        			case RuntimePlatform.Android:
        			case RuntimePlatform.IPhonePlayer:
        			case RuntimePlatform.tvOS:
        			case RuntimePlatform.WSAPlayerX86:
        			case RuntimePlatform.WSAPlayerX64:
        			case RuntimePlatform.WSAPlayerARM:
        			case RuntimePlatform.Stadia:
        			case RuntimePlatform.OpenHarmony:
        			#if UXX_2020_2_OR_NEWER
        			case RuntimePlatform.PS4:
        				#if !(UXX_2020_2_1 || UXX_2020_2_2)
        				case RuntimePlatform.PS5:
        				#endif
        			#endif
        			case RuntimePlatform.Switch:
        				SetPropertyUtility.SetStruct(ref m_HideSoftKeyboard, value);
        				break;
        			default:
        				m_HideSoftKeyboard = true;
        				break;
        		}
        
        		if (m_HideSoftKeyboard == true && m_SoftKeyboard != null && TouchScreenKeyboard.isSupported && m_SoftKeyboard.active)
        		{
        			m_SoftKeyboard.active = false;
        			m_SoftKeyboard = null;
        		}
        	}
        }
        ```

    3.  在RuntimePlatform.Android、UXX\_ANDROID、UXX\_MOBILE和shouldHideMobilelnput修改一致，在相应位置通过RuntimePlatform.OpenHarmony和UXX\_OPENHARMONY加上HarmonyOS平台的处理逻辑即可。

