# hdc install错误码9568332<a name="ZH-CN_TOPIC_0000001690307988"></a>

-   [问题描述](#section204041610291)
-   [解决方案](#section229518107295)

## 问题描述<a name="section204041610291"></a>

执行hdc install命令后出现如下报错信息：

```
error : install sign info inconsistent.
```

![](figures/image-(1)-(1).png)

## 解决方案<a name="section229518107295"></a>

删除测试机上与待安装包同名的包，再重新使用hdc命令安装Hap调试包即可。

