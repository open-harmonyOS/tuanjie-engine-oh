# 安装并配置DevEco Studio<a name="ZH-CN_TOPIC_0000001715014940"></a>

-   [申请DevEco Studio开发套件](#zh-cn_topic_0000001676851030_section18847185416347)
-   [下载并安装DevEco Studio](#zh-cn_topic_0000001676851030_section1176017203214)
-   [配置DevEco Studio开发环境](#zh-cn_topic_0000001676851030_section520715954418)
-   [学习DevEco Studio](#zh-cn_topic_0000001676851030_section1744485904113)
-   [熟悉ArkTS语言](#zh-cn_topic_0000001676851030_section1884358325)

## 申请DevEco Studio开发套件<a name="zh-cn_topic_0000001676851030_section18847185416347"></a>

1.  打开并使用华为帐号登录[官网](https://developer.harmonyos.com/deveco-developer-suite/)，在弹出的窗口中签署协议。
2.  请认真填写DevEco Studio开发套件的申请表单。

    ![](figures/20231031-105119(WeLinkPC).png)

3.  提交申请后，请向华为工作人员提供信息截图，华为工作人员会尽快处理您的申请。

## 下载并安装DevEco Studio<a name="zh-cn_topic_0000001676851030_section1176017203214"></a>

若DevEco Studio开发套件已成功申请，请前往[官网](https://developer.harmonyos.com/deveco-developer-suite/)下载**4.0.3.700**版本的DevEco Studio，并根据安装提示进行安装。

## 配置DevEco Studio开发环境<a name="zh-cn_topic_0000001676851030_section520715954418"></a>

您需**下载OpenHarmony SDK API Version 10**和**HarmonyOS SDK API Version 9**、**配置HDC工具环境变量**、**诊断开发环境**，详情请参考[配置开发环境](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/ide-environment-config-0000001507213638)。

![](figures/Snipaste_2023-09-06_10-51-33.png)

## 学习DevEco Studio<a name="zh-cn_topic_0000001676851030_section1744485904113"></a>

DevEco Studio是运行在HarmonyOS系统上的应用和服务提供一站式的开发平台，建议您参考[DevEco Studio使用指南](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/ide-tools-overview-0000001558763037)学习DevEco Studio工具的使用。

## 熟悉ArkTS语言<a name="zh-cn_topic_0000001676851030_section1884358325"></a>

DevEco Studio使用ArkTS语言开发，该语言是TS语言的超集，兼容绝大部分TS/JS语法，但对部分影响性能的TS语法做了约束，建议您参考[ArkTS语言](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/arkts-get-started-0000001580185142)完成相关基础语法的学习。

