# 传文件报错read-only file system<a name="ZH-CN_TOPIC_0000001703586252"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

执行file send命令将文件传送至测试机上时，出现如下报错信息：

```
[Fail]Error opening file: read-only file system, path:***
```

![](figures/a7dd0229d67eb9be95b8f79340ca9e1f_776x59-png-900-0-90-f.png)

## 解决方案<a name="section172643141482"></a>

由报错信息可以看出，无法传输文件是因为权限限制，因此您需要先获取读写权限。执行hdc命令获取读取权限。

```
hdc shell mount -o rw,remount /
```

