# 如何查看Hap包信息<a name="ZH-CN_TOPIC_0000001751425985"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

如何查看Hap调试包的信息，例如包名、bundleName等信息。

## 解决方案<a name="section172643141482"></a>

Hap调试包是一个压缩包，可以将其后缀修改为zip，再进行解压缩，查看文件夹中的module.json文件内容，参考[应用/组件级配置](https://developer.huawei.com/consumer/cn/doc/harmonyos-guides/application-component-configuration-stage-0000001630425073)获取包名、bundleName等信息。

