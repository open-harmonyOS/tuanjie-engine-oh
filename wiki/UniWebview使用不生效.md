# UniWebview使用不生效<a name="ZH-CN_TOPIC_0000001735237478"></a>

-   [问题描述](#section4541952195615)
-   [解决方案](#section13860125545615)

## 问题描述<a name="section4541952195615"></a>

由于UniWebview插件尚未适配HarmonyOS系统，游戏无法使用UniWebview实现网页加载等功能。

## 解决方案<a name="section13860125545615"></a>

现已有插件OpenHarmonyWebview实现了HarmonyOS平台的Webview能力，可通过引入该插件实现HarmonyOS平台的网页加载功能，接入指南请前往[Gitee网站](https://gitee.com/swallowguo/tuanjie-engine-oh/blob/master/plugin/Webview/Tuanjie/%E5%9B%A2%E7%BB%93%E5%BC%95%E6%93%8E%E5%AE%9E%E7%8E%B0%E9%B8%BF%E8%92%99%E5%B9%B3%E5%8F%B0webview%E8%83%BD%E5%8A%9B.pdf)下载并查看。

对于游戏脚本中调用UniWebview的地方，增加UXX\_OPENHARMONY宏分支进行控制，UXX\_OPENHARMONY分支调用OpenHarmonyWebview对应接口，将原有逻辑移至\#else分支。

```
#if UXX_OPENHARMONY
	OpenHarmonyWebview.MethodName();
#else
	原有逻辑
#endif
```

