# hdc shell安装大文件无反应<a name="ZH-CN_TOPIC_0000001741835773"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

在命令行窗口执行hdc install安装大于2.5G的文件时，命令行长时间无反应，且测试机上无安装包。

## 解决方案<a name="section10358174483919"></a>

原因是**hdc.exe**的版本较低，建议下载并使用SDK 10，并将HDC环境变量指向SDK 10的hdc.exe。

![](figures/Snipaste_2023-09-06_10-51-33-11.png)

