# 团结引擎无法找到DLL库<a name="ZH-CN_TOPIC_0000001703426752"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

团结引擎无法找到DLL库，且出现如下报错信息：

![](figures/c3725a9ced6f3f329376accf483fe049_1238x130-jpg-900-0-90-f.jpg)

## 解决方案<a name="section172643141482"></a>

在DLL库兼容平台属性窗口中设置HarmonyOS系统，设置成功后即可解决。

![](figures/95bb808a4c30482565b336f930933520_475x895.png)

