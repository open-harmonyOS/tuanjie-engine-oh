# hdc install报错fail to verify pkcs7 file<a name="ZH-CN_TOPIC_0000001693796182"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

执行hdc install命令后出现如下报错信息：

```
fail to verify pkcs7 file.
```

## 解决方案<a name="section10358174483919"></a>

1.  在命令行窗口执行如下命令进行规避：

    ```
    hdc shell param set persist.bms.ohCert.verify true
    ```

2.  您可以手动切换HarmonyOS证书，在DevEco Studio选择HarmonyOS并进行自动签名，也可以参考[手动签名方式](https://developer.huawei.com/consumer/cn/doc/app/agc-help-harmonyos-debugapp-manual-0000001177608893)进行手动签名。
3.  您可以进行如下操作进行验证：
    1.  将persist.bms.ohCert.verify设置为true, HarmonyOS证书签名的应用可以被安装。

        ```
        hdc shell param set persist.bms.ohCert.verify true
        ```

    2.  将persist.bms.ohCert.verify设置为false, HarmonyOS证书签名的应用不能被安装。
    3.  查询设置命令是否生效，若返回true，说明上面的命令已设置成功。

        ```
        hdc shell param get persist.bms.ohCert.verify
        ```

