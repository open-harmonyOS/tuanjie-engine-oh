# 团结引擎导出的项目在DevEco中打开报错<a name="ZH-CN_TOPIC_0000001729687385"></a>

-   [问题描述](#section2168104821413)
-   [解决方案](#section7961410150)

## 问题描述<a name="section2168104821413"></a>

从团结引擎导出的HarmonyOS工程，在DevEco Studio中一打开就出现如下报错：

```
Incorret settings found in the build-profile.json5 file.
```

![](figures/image-(6).png)

## 解决方案<a name="section7961410150"></a>

1.  在DevEco Studio中修改项目级build-profile.json5配置文件。将app下**products**字段修改为如下所示：

    ![](figures/zh-cn_image_0000001681834952.png)

    ```
    "products": [ 
    {   
        "name": "default",   
        "signingConfig": "default",   
        "compileSdkVersion": 10,     
        "compatibleSdkVersion": 10,      
        "targetSdkVersion": 10,          
        "runtimeOS": "OpenHarmony",    
    }
    ```

2.  完成后点击右上角的**Try Again**。
3.  若还出现如下提示，直接单击蓝色文字，切换**hvigor**版本即可。

    ![](figures/image-(7).png)

