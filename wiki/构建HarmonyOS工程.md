# 构建HarmonyOS工程<a name="ZH-CN_TOPIC_0000001714855496"></a>

完成上述操作后，请先在团结引擎中导出HarmonyOS工程，再在DevEco Studio中打开HarmonyOS工程。

1.  在团结引擎菜单栏选择“File\>Build Settings”，在弹出的窗口中勾选**Export Project**后，点击右下角**Export**，在弹出的窗口中选择HarmonyOS工程的保存路径。

    ![](figures/1_zh-cn_image_0000001727106517.png)

2.  在DevEco Studio中打开游戏工程。若在DevEco Studio中打开工程出现报错，您可以参考FAQ。

