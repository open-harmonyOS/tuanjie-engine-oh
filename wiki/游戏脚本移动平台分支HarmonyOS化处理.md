# 游戏脚本移动平台分支HarmonyOS化处理<a name="ZH-CN_TOPIC_0000001733955358"></a>

-   [问题描述](#section06061365373)
-   [解决方案](#section873759173710)
    -   [适配移动平台处理分支](#section7399114915589)
    -   [HarmonyOS化分支内部逻辑](#section126241610319)

## 问题描述<a name="section06061365373"></a>

如何适配移动平台处理分支及HarmonyOS化分支内的游戏逻辑。

## 解决方案<a name="section873759173710"></a>

### 适配移动平台处理分支<a name="section7399114915589"></a>

在不同的平台处理分支类型中增加HarmonyOS平台有不同的方法，具体如下：

-   **类型一**：如下，Android、iOS移动平台统一处理分支，您可直接在判断条件处增加UXX\_OPENHARMONY。

    ```
    #if UXX_ANDROID || UXX_IOS
    	...
    #endif
    
    #if !UXX_ANDROID || !UNTIY_IOS
    	...
    #endif
    ```

-   **类型二**：如下，Android、iOS移动平台分别处理分支，您需结合业务逻辑分析具体功能，再增加HarmonyOS分支处理。若平台分支各自实现原生库，则HarmonyOS系统也需利用原生语言实现功能，再封装好提供给UXX调用。

    ```
    #if UXX_ANDROID
    	...
    #elif UXX_IOS
    	...
    #endif
    
    #if UXX_ANDROID
    	...
    #endif
    #if UXX_IOS
    	...
    #endif
    ```

-   **类型三**：如下，仅有Android移动平台处理分支，此类代码一般仅针对Android平台的特定逻辑，您需结合具体功能判断HarmonyOS平台是否需要实现此功能。若存在**else**分支，代码走读时需判别HarmonyOS系统走此分支是否存在问题。

    ```
    #if !UXX_ANDROID 
       ...
    #else
       ...
    #endif  
    
    #if UXX_ANDROID 
       ...
    #endif
    
    #if UXX_ANDROID
       ...
    #else
       ...
    #endif 
    
    #if  UXX_ANDROID && otherConditions
       ...
    #elif UXX_ANDROID && !otherConditions 
       ...
    #endif
    ```

-   **类型四**：如下，仅有iOS移动平台处理分支，是否添加HarmonyOS平台的分支处理原则同**类型三**一致。

    ```
    #if !UXX_IOS 
       ...
    #else 
       ...
    #endif  
    
    #if UXX_IOS 
       ...
    #endif
    
    #if UXX_IOS
       ...
    #else
       ...
    #endif 
    
    #if  UXX_IOS && otherCondition
       ...
    #elif UXX_IOS && !otherCondition
       ...
    #endif
    ```

### HarmonyOS化分支内部逻辑<a name="section126241610319"></a>

移动平台处理分支的内部逻辑分为**纯业务处理**和**三方库交互**：

-   对于纯业务处理，您需要根据游戏功能决定是否添加HarmonyOS平台的处理逻辑。
-   对于三方库交互，您需要根据三方库的类型分类处理：
    -   **C/C++库**。如下示例为C/C++库常见的平台宏处理代码形式，此形式是为了单独处理某个平台对于C/C++库的静态链接。若想适配HarmonyOS平台，您需要使用HarmonyOS编译工具链重新编译C/C++库，实现逻辑走**else**分支即可。

        ```
        #if UXX_IOS
        	[DllImport("__internal__")]
        	...
        #else
        	[DllImport("{libname}")]
        	...
        #endif
        ```

    -   **平台原生库**。

        Android平台使用Java语言开发，iOS平台使用Objective-C语言开发。平台原生库对应上述移动平台处理分支的类型二、类型三和类型四。

        <a name="table77831165166"></a>
        <table><thead align="left"><tr id="row87848617169"><th class="cellrowborder" valign="top" width="30%" id="mcps1.1.4.1.1"><p id="p978415671615"><a name="p978415671615"></a><a name="p978415671615"></a>移动平台处理分支类型</p>
        </th>
        <th class="cellrowborder" valign="top" width="30%" id="mcps1.1.4.1.2"><p id="p67849610166"><a name="p67849610166"></a><a name="p67849610166"></a>总结分支处理特点</p>
        </th>
        <th class="cellrowborder" valign="top" width="40%" id="mcps1.1.4.1.3"><p id="p2078416651616"><a name="p2078416651616"></a><a name="p2078416651616"></a>适配平台原生库的步骤</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row2784106121616"><td class="cellrowborder" valign="top" width="30%" headers="mcps1.1.4.1.1 "><p id="p137843671611"><a name="p137843671611"></a><a name="p137843671611"></a>类型二（Android、iOS移动平台分别处理分支）</p>
        </td>
        <td class="cellrowborder" valign="top" width="30%" headers="mcps1.1.4.1.2 "><p id="p47842651618"><a name="p47842651618"></a><a name="p47842651618"></a>直接与平台原生库进行交互。</p>
        </td>
        <td class="cellrowborder" valign="top" width="40%" headers="mcps1.1.4.1.3 "><a name="ol16767650162017"></a><a name="ol16767650162017"></a><ol id="ol16767650162017"><li>使用HarmonyOS应用开发语言重新编写代码。</li><li>添加HarmonyOS分支调用原生库。</li></ol>
        </td>
        </tr>
        <tr id="row1478410621614"><td class="cellrowborder" valign="top" width="30%" headers="mcps1.1.4.1.1 "><p id="p157844611618"><a name="p157844611618"></a><a name="p157844611618"></a>类型三（仅有Android移动平台处理分支）</p>
        </td>
        <td class="cellrowborder" rowspan="2" valign="top" width="30%" headers="mcps1.1.4.1.2 "><p id="p16784166121619"><a name="p16784166121619"></a><a name="p16784166121619"></a>使用C#类封装了具体平台与平台原生库的交互，分支内部一般仅设计具体平台的宏处理。</p>
        </td>
        <td class="cellrowborder" rowspan="2" valign="top" width="40%" headers="mcps1.1.4.1.3 "><a name="ol9168313142115"></a><a name="ol9168313142115"></a><ol id="ol9168313142115"><li>使用HarmonyOS应用开发语言重新编写代码。</li><li>添加HarmonyOS系统交互C#类，内部使用UXX_OPENHARMONY进行控制，在C#业务控制逻辑中增加UXX_OPENHARMONY分支控制HarmonyOS系统交互C#类的调用。</li></ol>
        </td>
        </tr>
        <tr id="row887832919162"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p20878162941612"><a name="p20878162941612"></a><a name="p20878162941612"></a>类型四（仅有iOS移动平台处理分支）</p>
        </td>
        </tr>
        </tbody>
        </table>

