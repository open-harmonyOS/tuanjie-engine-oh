# 如何获取团结引擎<a name="ZH-CN_TOPIC_0000001703426748"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

如何获取团结引擎？

## 解决方案<a name="section172643141482"></a>

前往[官网](https://unity.cn/tuanjie/tuanjieyinqing)，点击“联系我们”，填写引擎的申请表格。

>![](public_sys-resources/icon-note.gif) **说明：** 
>引擎下发时间以官方为准。

![](figures/未标题-1-1.png)

