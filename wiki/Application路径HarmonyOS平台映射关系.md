# Application路径HarmonyOS平台映射关系<a name="ZH-CN_TOPIC_0000001780835629"></a>

-   [问题描述](#section06061365373)
-   [解决方案](#section873759173710)

## 问题描述<a name="section06061365373"></a>

团结引擎Application路径对应的项目工程、平台文件系统具体位置是什么？

## 解决方案<a name="section873759173710"></a>

-   Application.streamingAssetsPath对应DevEco Studio工程路径是\*\*/entry/src/main/resources/rawfile/Data/StreamingAssets\*\*，在使用OpenHarmony API 9设备上的路径为data/storage/el1/bundle/entry/resources/rawfile/Data/StreamingAssets（应用沙箱路径，非文件系统实际路径），在API 10的设备上该路径已不可见。Application.dataPath对应Application.streamingAssetsPath的上一级目录。
-   Application.persistentPath的应用沙箱路径是/data/storage/el2/base/haps/entry/files，文件系统实际路径为`/data/app/el2/100/base//haps/entry/files `。Application对应同级的cache目录。

    ![](figures/7e2a76fc43d6ee044c7c4234190c8eea_687x596.png)

您可以使用如下命令在文件传输时使用文件系统实际路径。

```
hdc file send
```

Application路径与HarmonyOS平台取值的对应关系总结为下表：

<a name="table20337173918382"></a>
<table><thead align="left"><tr id="row633816397388"><th class="cellrowborder" align="left" valign="top" width="50%" id="mcps1.1.3.1.1"><p id="p7291135012416"><a name="p7291135012416"></a><a name="p7291135012416"></a>Application路径</p>
</th>
<th class="cellrowborder" align="left" valign="top" width="50%" id="mcps1.1.3.1.2"><p id="p13291175018415"><a name="p13291175018415"></a><a name="p13291175018415"></a>HarmonyOS平台取值</p>
</th>
</tr>
</thead>
<tbody><tr id="row3338039103819"><td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p id="p929265017415"><a name="p929265017415"></a><a name="p929265017415"></a>Application.streamingAssetsPath</p>
</td>
<td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p id="p1629255054118"><a name="p1629255054118"></a><a name="p1629255054118"></a>rawfile/Data/StreamingAssets</p>
</td>
</tr>
<tr id="row123381839153819"><td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p id="p32920506419"><a name="p32920506419"></a><a name="p32920506419"></a>Application.persistentPath</p>
</td>
<td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p id="p1929215502410"><a name="p1929215502410"></a><a name="p1929215502410"></a>/data/storage/el2/base/haps/entry/files</p>
</td>
</tr>
<tr id="row113381639193815"><td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p id="p729211505419"><a name="p729211505419"></a><a name="p729211505419"></a>Application.dataPath</p>
</td>
<td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p id="p142921750194120"><a name="p142921750194120"></a><a name="p142921750194120"></a>rawfile/Data</p>
</td>
</tr>
<tr id="row761174220411"><td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.1 "><p id="p22923508415"><a name="p22923508415"></a><a name="p22923508415"></a>Application.temporaryCachePath</p>
</td>
<td class="cellrowborder" align="left" valign="top" width="50%" headers="mcps1.1.3.1.2 "><p id="p14292650114115"><a name="p14292650114115"></a><a name="p14292650114115"></a>/data/storage/el2/base/haps/entry/cache</p>
</td>
</tr>
</tbody>
</table>

