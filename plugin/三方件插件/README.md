# 说明
本目录存放团结引擎三方件对应的插件，持续更新

# 目录介绍
**FMOD**：<br>
为鸿蒙化FMOD SDK提供接入层插件代码，so库接口通过TS代码完成一次封装提供给C#层调用。<br>
详细介绍见指导文档：<a href="https://gitee.com/swallowguo/tuanjie-engine-oh/blob/master/plugin/%E4%B8%89%E6%96%B9%E4%BB%B6%E6%8F%92%E4%BB%B6/FMOD/OpenHarmony%E6%8E%A5%E5%85%A5FMOD%E6%8C%87%E5%AF%BC.docx" target="_blank">《OpenHarmony接入FMOD指导.docx》
<a/>