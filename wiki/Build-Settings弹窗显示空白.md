# Build Settings弹窗显示空白<a name="ZH-CN_TOPIC_0000001751306157"></a>

-   [问题描述](#section17111494812)
-   [解决方案](#section172643141482)

## 问题描述<a name="section17111494812"></a>

团结引擎的Build Settings弹窗显示空白，且出现如下错误信息：

![](figures/49f3199a3b68d86ff0f4d7be81ebe62a_1352x355.png)

## 解决方案<a name="section172643141482"></a>

请参考[官网](https://forum.unity.com/threads/unable-to-load-the-icon-cacheserverdisabled-problem-occurring.828741/#post-5503402)在对应目录设置图标。

