# 如何读取StreamingAssets文件夹内容<a name="ZH-CN_TOPIC_0000001751306169"></a>

-   [问题描述](#section17111494812)

## 问题描述<a name="section17111494812"></a>

使用UnityWebRequest.Get\(url\)方式读取StreamingAsset文件夹内容。

```
var url = Application.streamingAssetsPath + version.file;
var savePath = Application.persistentDataPath +(version.file);
var webRequest = UnityWebRequest.Get(url);
webRequest.downloadHandler =newDownloadHandlerFile(savePath);
yieldreturn webRequest.SendWebRequest();
```



