# 打包是否勾选Development Build<a name="ZH-CN_TOPIC_0000001693955682"></a>

-   [问题描述](#section36019390395)
-   [解决方案](#section10358174483919)

## 问题描述<a name="section36019390395"></a>

团结引擎构建Hap调试包时，是否需要勾选Development Build选项。

![](figures/zh-cn_image_0000001751532657.png)

## 解决方案<a name="section10358174483919"></a>

-   若勾选Development Build，将依赖../Variations/il2cpp/development。
-   若不勾选Development Build，将依赖../Variations/il2cpp/release。

您可以先查看是否存在相关目录，再决定是否勾选Development Build选项。

