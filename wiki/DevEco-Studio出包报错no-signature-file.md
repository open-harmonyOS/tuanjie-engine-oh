# DevEco Studio出包报错no signature file<a name="ZH-CN_TOPIC_0000001780835625"></a>

-   [问题描述](#section10567155814614)
-   [解决方案](#section8659225711)

## 问题描述<a name="section10567155814614"></a>

DevEco Studio编译出包时出现如下报错信息：

```
error: no signature file.
```

![](figures/image-4.png)

## 解决方案<a name="section8659225711"></a>

在DevEco Studio的Project Structure弹窗中生成自动签名信息。

![](figures/image-(1)-5.png)

