# hdc install错误码9568274<a name="ZH-CN_TOPIC_0000001745988525"></a>

-   [问题描述](#section10683154310571)
-   [解决方案](#section2437955105713)

## 问题描述<a name="section10683154310571"></a>

执行hdc install命令后出现如下报错信息：

```
failed to install bundle，code：9568274。
```

## 解决方案<a name="section2437955105713"></a>

您需重启测试机，再执行hdc install命令。

