# 环境配置<a name="ZH-CN_TOPIC_0000001724616433"></a>

-   **[如何获取团结引擎](如何获取团结引擎.md)**  

-   **[打包是否勾选Development Build](打包是否勾选Development-Build.md)**  

-   **[打包出现Bee乱码](打包出现Bee乱码.md)**  

-   **[配置SDK出现格式错误](配置SDK出现格式错误.md)**  

-   **[如何申请DevEco Studio套件](如何申请DevEco-Studio套件.md)**  

-   **[打包显示找不到node](打包显示找不到node.md)**  

-   **[Build Settings弹窗显示空白](Build-Settings弹窗显示空白.md)**  

